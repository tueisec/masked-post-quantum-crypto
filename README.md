# Masked Accelerators and Instruction Set Extensions for Post-Quantum Cryptography
This repository will provide masked hardware accelerators and instruction set extensions for post-quantum cryptography.
The work is based on the article: "Masked Accelerators and Instruction Set Extensions for Post-Quantum Cryptography" (https://tches.iacr.org/index.php/TCHES/article/view/9303), which was accepted at the journal TCHES 2022.

The files will be uploaded as soon as the permission for the upload has been granted. Our regulations require a corresponding approval.
